<?php

/**
 * Implements base_settings_form()
 */

function commerce_amount_coupon_pane_settings_form($checkout_pane) {
  // Load the default currency for this instance.
  $default_currency = commerce_default_currency();
  $currency = commerce_currency_load($default_currency);
  // Round the default value.
  $default_amount = commerce_currency_amount_to_decimal('0',$currency['code']);
  $form = array();
  $form['commerce_amount_coupon_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupon Price'),
    '#size' => 10,
    '#default_value' => $default_amount,
    '#field_suffix' => $currency['code'],
  );
  $form['commerce_amount_coupon_pane_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupon code'),
    '#default_value' => variable_get('commerce_amount_coupon_pane_field', ''),
  );
  return $form;
}

/**
 * Custom submit function to handle the Commerce Amount Coupon Price.
 */

function commerce_amount_coupon_pane_settings_form_submit($form, &$form_state) {
  // Load the default currency for this instance.
  $default_currency = commerce_default_currency();
  $currency = commerce_currency_load($default_currency);
  // Get the decimal price.
  $decimal_price = $form_state['values']['commerce_amount_coupon_price'];
  // Convert decimal price to amount for storage.
  $amount_price = commerce_currency_decimal_to_amount($decimal_price, $currency['code']);
  variable_set('commerce_amount_coupon_price', $amount_price);
}

/**
 * Implements base_checkout_form()
 */

function commerce_amount_coupon_pane_checkout_form($form, $form_state, $checkout_pane, $order) {

	$checkout_form['commerce_amount_coupon_pane_field_display'] = array(
	  '#markup' => variable_get('commerce_amount_coupon_pane_field', ''),
	);

	$checkout_form['commerce_amount_coupon_pane_coupon_code'] = array(
		'#type' => 'textfield',
	  '#title' => t('Please enter your coupon code!'),
		'#element_validate' => array('coupon_code_validation'),
	);

	return $checkout_form;

}

/**
 * Implements base_checkout_form_submit().
 */

function commerce_amount_coupon_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  $amount_coupon_line_item_exists = commerce_amount_coupon_line_item_exists($order);
	$amount_coupon_decision = variable_get('commerce_amount_coupon_decision');

  if ($amount_coupon_decision) {
    if ($amount_coupon_line_item_exists) {
      // Update the line item if message has been changed.
      $line_item_id = commerce_amount_coupon_get_line_item_id($order);
      $line_item = commerce_line_item_load($line_item_id);
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      // Save the line item..
      commerce_line_item_save($line_item);
    }
    elseif (!$amount_coupon_line_item_exists) {
      // Set the currency code.
      $default_currency_code = commerce_default_currency();
      if ($balance = commerce_payment_order_balance($order)) {
        $default_currency_code = $balance['currency_code'];
      }
      // Create the new line item.
      $line_item = commerce_line_item_new('commerce_amount_coupon', $order->order_id);
      // Wrap the line item and order to simplify manipulating their field data.
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      // Populate the $line_item_wrapper...
      $line_item_wrapper->line_item_label = t('Coupon') . ' ' . variable_get('commerce_amount_coupon_code');
      $line_item_wrapper->quantity = 1;
      $line_item_wrapper->commerce_unit_price->amount = variable_get('commerce_amount_coupon_price');
      $line_item_wrapper->commerce_unit_price->currency_code = $default_currency_code;
      // Set the price component of the unit price.
      $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
        $line_item_wrapper->commerce_unit_price->value(),
        'commerce_amount_coupon',
        $line_item_wrapper->commerce_unit_price->value(),
        TRUE,
        FALSE
      );
      // Save the incoming line item now so we get its ID.
      commerce_line_item_save($line_item);
      // Add it to the order's line item reference value.
      $order_wrapper->commerce_line_items[] = $line_item;
    }
    // Save the order.
    commerce_order_save($order);
  }
  elseif (!$amount_coupon_decision) {
    // If the user selects 'No' check if coupon has been added, if so
    // delete it.
    commerce_amount_coupon_delete_commerce_amount_coupon_line_items($order);
  }
}
